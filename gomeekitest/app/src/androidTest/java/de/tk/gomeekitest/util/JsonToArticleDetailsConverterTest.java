package de.tk.gomeekitest.util;

import junit.framework.TestCase;

import org.json.JSONArray;
import org.json.JSONObject;

import de.tk.gomeekitest.model.ArticleDetails;

/**
 * Created by thimokluser on 30/12/15.
 */
public class JsonToArticleDetailsConverterTest extends TestCase {

    public void testConvert() throws Exception {
        JSONObject coordinates = new JSONObject();
        coordinates.put("lat", "-36.8484600");
        coordinates.put("long", "174.7633320");

        JSONObject data = new JSONObject();
        data.put("content", "Lorem ipsum");
        data.put("date", "20140212T102819+1100");
        data.put("id", "ID");
        data.put("title", "TITLE");
        data.put("image", "/image/4972866-16x9-700x394.jpg");
        data.put("coordinates", coordinates);

        JSONObject status = new JSONObject();
        status.put("code", "200");

        JSONObject root = new JSONObject();
        root.put("status", status);
        root.put("data", data);

        ArticleDetails details = JsonToArticleDetailsConverter.convert(root);

        assertNotNull(details);
        assertEquals(coordinates.get("lat"), details.getLat());
        assertEquals(coordinates.get("long"), details.getLon());

        assertEquals(data.get("content"), details.getContent());
        assertEquals(data.get("title"), details.getTitle());
        assertEquals(data.get("image"), details.getMedia());
    }
}