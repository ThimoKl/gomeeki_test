package de.tk.gomeekitest.util;

import junit.framework.TestCase;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import de.tk.gomeekitest.model.Article;

/**
 * Created by thimokluser on 30/12/15.
 */
public class JsonToArticleListConverterTest extends TestCase {

    public void testConvert() throws Exception {
        JSONObject a1 = new JSONObject();
        a1.put("id", "123");
        a1.put("date", "DATE");
        a1.put("title", "TITLE");
        a1.put("short_description", "DESCRIPTION");
        a1.put("image", "URL");

        JSONObject a2 = new JSONObject();
        a2.put("id", "456");
        a2.put("date", "DATE2");
        a2.put("title", "TITLE2");
        a2.put("short_description", "DESCRIPTION2");
        a2.put("image", "URL2");

        JSONArray articles = new JSONArray();
        articles.put(a1);
        articles.put(a2);

        JSONObject data = new JSONObject();
        data.put("articles", articles);

        JSONObject status = new JSONObject();
        status.put("code", "200");

        JSONObject root = new JSONObject();
        root.put("status", status);
        root.put("data", data);

        List<Article> result = JsonToArticleListConverter.convert(root);

        // Check list size
        assertEquals("List size should be 2.", 2, result.size());

        // Check article 1
        Article article1 = result.get(0);
        assertEquals("IDs should be equal.", a1.get("id"), article1.getId());
        assertEquals("Titles should be equal.", a1.get("title"), article1.getTitle());
        assertEquals("Dates should be equal.", a1.get("date"), article1.getDate());
        assertEquals("Short descriptions should be equal.", a1.get("short_description"), article1.getShortDescription());
        assertEquals("Images should be equal.", a1.get("image"), article1.getImage());

        // Check article 2
        Article article2 = result.get(1);
        assertEquals("IDs should be equal.", a2.get("id"), article2.getId());
        assertEquals("Titles should be equal.", a2.get("title"), article2.getTitle());
        assertEquals("Dates should be equal.", a2.get("date"), article2.getDate());
        assertEquals("Short descriptions should be equal.", a2.get("short_description"), article2.getShortDescription());
        assertEquals("Images should be equal.", a2.get("image"), article2.getImage());

        // Check status code
        status.put("code", "500");
        result = JsonToArticleListConverter.convert(root);

        assertNull("Bad status code -> Article list should be null", result);
    }
}