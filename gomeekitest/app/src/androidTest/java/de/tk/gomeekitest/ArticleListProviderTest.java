package de.tk.gomeekitest;

import com.android.volley.VolleyError;

import junit.framework.TestCase;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import de.tk.gomeekitest.model.Article;

/**
 * Created by thimokluser on 30/12/15.
 */
public class ArticleListProviderTest extends TestCase {

    public void testRequestArticles() throws Exception {
        String correctUrl = "http://mobiletest.gomeekisystems.com/category_sport.json";
        final CountDownLatch lock1 = new CountDownLatch(1);

        ArticleListProvider.requestArticles(correctUrl,
                new ArticleListProvider.OnArticlesDownloadedListener() {

                    @Override
                    public void onArticlesDownloaded(List<Article> articles) {
                        assertNotNull("Correct url, List shouldn't be null", articles);
                        assertTrue("Correct url, List size should be bigger than 0", articles.size() > 0);
                        lock1.countDown();
                    }

                    @Override
                    public void onArticlesDownloadFailed(VolleyError error) {
                        assertTrue("Download failed. It shouldn't fail, unless the test device has connectivity issues.", false);
                        lock1.countDown();
                    }
                });
        assertEquals(true, lock1.await(10000, TimeUnit.MILLISECONDS));

        String incorrectUrl = "http://www.google.com";
        final CountDownLatch lock2 = new CountDownLatch(1);

        ArticleListProvider.requestArticles(incorrectUrl,
                new ArticleListProvider.OnArticlesDownloadedListener() {

                    @Override
                    public void onArticlesDownloaded(List<Article> articles) {
                        assertTrue("Wrong URL, it shouldn't convert articles", false);
                        lock2.countDown();
                    }

                    @Override
                    public void onArticlesDownloadFailed(VolleyError error) {
                        lock2.countDown();
                    }
                });
        assertEquals(true, lock2.await(10000, TimeUnit.MILLISECONDS));
    }
}