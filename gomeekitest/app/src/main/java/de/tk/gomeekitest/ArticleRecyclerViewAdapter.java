package de.tk.gomeekitest;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import de.tk.gomeekitest.model.Article;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Article} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 */
public class ArticleRecyclerViewAdapter extends RecyclerView.Adapter<ArticleRecyclerViewAdapter.ViewHolder> {

    private List<Article> mValues;
    private final OnListFragmentInteractionListener mListener;

    public ArticleRecyclerViewAdapter(List<Article> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    /**
     * Update the list with new articles
     *
     * @param items List of articles
     */
    public void changeDataSet(List<Article> items) {
        mValues = items;
        notifyDataSetChanged();
    }

    /**
     * Get all articles
     * @return List of articles
     */
    public List<Article> getItems() {
        return mValues;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.article_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mTxtTitle.setText(mValues.get(position).getTitle());
        holder.mTxtShortDesc.setText(mValues.get(position).getShortDescription());

        String imageUrl = mValues.get(position).getImage();
        boolean loadImage = (imageUrl.length() != 0);
        holder.mIvThumbnail.setVisibility(loadImage ? View.VISIBLE : View.GONE);

        if(loadImage) {
            // Lazy load image
            Context context = holder.mView.getContext();
            Picasso.with(context)
                    .load(context.getString(R.string.base_url) + imageUrl).into(holder.mIvThumbnail);
        }

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onArticleSelected(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mTxtTitle;
        public final TextView mTxtShortDesc;
        public final ImageView mIvThumbnail;

        public Article mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mTxtTitle = (TextView) view.findViewById(R.id.txtTitle);
            mTxtShortDesc = (TextView) view.findViewById(R.id.txtDescription);
            mIvThumbnail = (ImageView) view.findViewById(R.id.ivThumbnail);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mTxtTitle.getText() + "'";
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     */
    public interface OnListFragmentInteractionListener {
        void onArticleSelected(Article item);
    }
}
