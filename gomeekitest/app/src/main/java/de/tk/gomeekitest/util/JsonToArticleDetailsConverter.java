package de.tk.gomeekitest.util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.tk.gomeekitest.model.Article;
import de.tk.gomeekitest.model.ArticleDetails;

/**
 * Convert an article represented by a {@link JSONObject} to an {@link ArticleDetails} objects.
 */
public class JsonToArticleDetailsConverter {

    /**
     * Convert an article represented by a {@link JSONObject} to an {@link ArticleDetails} objects.
     * @param jsonArticleDetails Article details as JSONObject
     * @return Details of an article
     */
    public static ArticleDetails convert(JSONObject jsonArticleDetails) {
        if(jsonArticleDetails == null) return null;

        ArticleDetails result = new ArticleDetails();

        try {
            if(!jsonArticleDetails.getJSONObject("status").getString("code").equals("200")) return null;

            JSONObject details = jsonArticleDetails.getJSONObject("data");

            result.setTitle(details.getString("title"));
            result.setMedia(details.getString("image"));
            result.setContent(details.getString("content"));

            JSONObject coordinates = details.getJSONObject("coordinates");
            result.setLat(coordinates.getString("lat"));
            result.setLon(coordinates.getString("long"));

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

        return result;
    }
}
