package de.tk.gomeekitest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

/**
 * MainActivity
 * This activity contains the article list fragment
 *
 * The list logic is implemented in the fragment, in case this activity gets extended.
 * E.g. a category selection on the left with the content on the right (Master/slave).
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

}
