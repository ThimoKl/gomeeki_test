package de.tk.gomeekitest.util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.tk.gomeekitest.model.Article;

/**
 * Convert an article list represented by a {@link org.json.JSONObject} to an ArrayList of Articles.
 */
public class JsonToArticleListConverter {

    /**
     * Converts a article list represented by a {@link org.json.JSONObject} to an ArrayList of Articles.
     * @param jsonArticles Articles as JSONObject
     * @return List of {@link de.tk.gomeekitest.model.Article}
     */
    public static List<Article> convert(JSONObject jsonArticles) {
        if(jsonArticles == null) return null;

        List<Article> result = new ArrayList<Article>();

        try {
            if(!jsonArticles.getJSONObject("status").getString("code").equals("200")) return null;

            JSONArray articlesArray = jsonArticles.getJSONObject("data").getJSONArray("articles");

            for(int i=0; i<articlesArray.length(); i++) {
                JSONObject jsonArticle = articlesArray.getJSONObject(i);

                Article article = new Article();
                article.setId(jsonArticle.getString("id"));
                article.setDate(jsonArticle.getString("date"));
                article.setTitle(jsonArticle.getString("title"));
                article.setShortDescription(jsonArticle.getString("short_description"));
                article.setImage(jsonArticle.getString("image"));

                result.add(article);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

        return result;
    }
}
