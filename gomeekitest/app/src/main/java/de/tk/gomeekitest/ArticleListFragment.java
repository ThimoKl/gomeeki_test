package de.tk.gomeekitest;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import de.tk.gomeekitest.model.Article;

/**
 * A fragment representing a list of articles.
 */
public class ArticleListFragment extends Fragment implements
        ArticleListProvider.OnArticlesDownloadedListener,
        ArticleRecyclerViewAdapter.OnListFragmentInteractionListener {

    protected ArticleRecyclerViewAdapter mAdapter;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ArticleListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_article_list, container, false);
        Context context = view.getContext();

        // Set the adapter
        RecyclerView recyclerView = (RecyclerView)view.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        mAdapter = new ArticleRecyclerViewAdapter(new ArrayList<Article>(), this);
        recyclerView.setAdapter(mAdapter);

        if(savedInstanceState != null && savedInstanceState.containsKey("articles")) {
            // Restore instance state
            ArrayList<Article> articles = savedInstanceState.getParcelableArrayList("articles");
            onArticlesDownloaded(articles);
        } else {
            // Download all articles
            downloadArticles();
        }

        // Make error view clickable
        View errorView = view.findViewById(R.id.error);
        errorView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadArticles();
            }
        });

        return view;
    }

    /**
     * Send the request to download all articles
     */
    private void downloadArticles() {
        // Download all articles
        ArticleListProvider.requestArticles(getString(R.string.category_url), this);
    }

    /**
     * Downloaded all articles
     *
     * @param articles List of {@link Article}
     */
    @Override
    public void onArticlesDownloaded(List<Article> articles) {
        getView().findViewById(R.id.error).setVisibility(View.GONE);
        getView().findViewById(R.id.list).setVisibility(View.VISIBLE);

        Collections.sort(articles, new Comparator<Article>() {
            public int compare(Article m1, Article m2) {
                return m2.getDateObject().compareTo(m1.getDateObject());
            }
        });

        mAdapter.changeDataSet(articles);
    }

    /**
     * Something went wrong.
     * The download failed.
     * Show how to try again
     *
     * @param error {@link VolleyError} object that describes what went wront.
     */
    @Override
    public void onArticlesDownloadFailed(VolleyError error) {
        getView().findViewById(R.id.list).setVisibility(View.GONE);
        getView().findViewById(R.id.error).setVisibility(View.VISIBLE);
    }

    /**
     * The user clicked on an article. Open the detail view.
     *
     * @param item Article
     */
    @Override
    public void onArticleSelected(Article item) {
        Intent intent = new Intent(getActivity(), ArticleActivity.class);
        intent.putExtra("articleId", item.getId());
        intent.putExtra("title", item.getTitle());
        getActivity().startActivity(intent);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        ArrayList<Article> articles = (ArrayList<Article>) mAdapter.getItems();

        outState.putParcelableArrayList("articles", articles);

        super.onSaveInstanceState(outState);
    }
}