package de.tk.gomeekitest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import de.tk.gomeekitest.model.ArticleDetails;
import de.tk.gomeekitest.util.JsonToArticleDetailsConverter;
import de.tk.gomeekitest.util.JsonToArticleListConverter;

/**
 * This activity display the content of one article
 */
public class ArticleActivity extends AppCompatActivity {

    private ArticleDetails mDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article);
        restoreInstanceState(savedInstanceState);

        Intent intent = getIntent();
        String articleId = intent.getExtras().getString("articleId");
        if(articleId == null) finish();

        String title = intent.getExtras().getString("title");
        getSupportActionBar().setTitle(title);

        if(mDetails == null) {
            downloadDetails(articleId);
        } else {
            onDetailsDownloaded(mDetails);
        }

    }


    /**
     * Inflate the options menu.
     *
     * @param menu Menu
     * @return Always true
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_article_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * The map menu item was clicked.
     * Open the map activity
     * @param item The clicked menu item
     */
    public void openMapClicked(MenuItem item){
        Intent intent = new Intent(this, MapsActivity.class);
        intent.putExtra("lat", mDetails.getLat());
        intent.putExtra("lon", mDetails.getLon());
        intent.putExtra("title", mDetails.getTitle());
        startActivity(intent);
    }

    /**
     * Download the article details and convert them from JSON to {@link ArticleDetails}.
     * @param articleId Article ID
     */
    private void downloadDetails(String articleId) {
        String url = getString(R.string.base_url) + "/" + articleId + ".json";

        JsonObjectRequest jsonObjRequest = new JsonObjectRequest(Request.Method.GET, url,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        onDetailsDownloaded(JsonToArticleDetailsConverter.convert(response));
                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                onDownloadFailed();
            }

        });
        MyApplication.getInstance().addToRequestQueue(jsonObjRequest);
    }

    /**
     * The article details are downloaded.
     * Display everything
     *
     * @param details Article details
     */
    private void onDetailsDownloaded(ArticleDetails details) {

        mDetails = details;

        final ImageView ivMedia = (ImageView) findViewById(R.id.ivMedia);
        if(details.getMedia().length() >= 0) {
            Picasso.with(this)
                    .load(getString(R.string.base_url) + details.getMedia())
                    .into(ivMedia);
        } else {
            ivMedia.setVisibility(View.GONE);
        }

        final WebView wvDescription = (WebView) findViewById(R.id.wvDescription);
        wvDescription.loadData(details.getContent(), "text/html", "UTF-8");
    }

    /**
     * Woops, something went wrong.
     * Close the details activity.
     */
    private void onDownloadFailed() {
        finish();
    }

    /**
     * Restore the instance state
     *
     * @param in Instance state
     */
    private void restoreInstanceState(Bundle in) {
        if(in == null) return;
        mDetails = new ArticleDetails();
        mDetails.setMedia(in.getString("media"));
        mDetails.setLat(in.getString("lat"));
        mDetails.setLon(in.getString("lon"));
        mDetails.setContent(in.getString("content"));
    }

    /**
     * Save the instance state for configuration changes
     * @param outState Instance state
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("media", mDetails.getMedia());
        outState.putString("lat", mDetails.getLat());
        outState.putString("lon", mDetails.getLon());
        outState.putString("content", mDetails.getContent());
        super.onSaveInstanceState(outState);
    }
}
