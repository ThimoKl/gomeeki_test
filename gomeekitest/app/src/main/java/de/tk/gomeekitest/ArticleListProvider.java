package de.tk.gomeekitest;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.List;

import de.tk.gomeekitest.model.Article;
import de.tk.gomeekitest.util.JsonToArticleListConverter;

/**
 * Download a list of articles from this JSON API:
 * http://mobiletest.gomeekisystems.com/category_sport.json
 */
public class ArticleListProvider {

    /**
     * Notify when the download finished or failed.
     */
    public interface OnArticlesDownloadedListener  {
        /**
         * Downloaded all articles
         * @param articles List of {@link Article}
         */
        void onArticlesDownloaded(List<Article> articles);

        /**
         * Something went wrong.
         * The download failed.
         *
         * @param error {@link VolleyError} object that describes what went wront.
         */
        void onArticlesDownloadFailed(VolleyError error);
    }

    /**
     * Request the list of articles.
     *
     * @param url URL of the API. It is stored in R.strings.category_url
     * @param listener Success or error listener
     */
    public static void requestArticles(String url,
                                       final OnArticlesDownloadedListener listener) {
        JsonObjectRequest jsonObjRequest = new JsonObjectRequest(Request.Method.GET, url,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        if(listener == null) return;
                        listener.onArticlesDownloaded(
                                JsonToArticleListConverter.convert(response));
                    }

                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(listener == null) return;
                        listener.onArticlesDownloadFailed(error);
                    }

                });
        MyApplication.getInstance().addToRequestQueue(jsonObjRequest);
    }

}
