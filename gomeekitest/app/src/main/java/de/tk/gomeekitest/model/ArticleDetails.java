package de.tk.gomeekitest.model;

/**
 * Model: ArticleDetails
 *
 * This model represents the details of one article that is displayed with the
 * {@link de.tk.gomeekitest.ArticleActivity}.
 */
public class ArticleDetails {

    private String mTitle = "";
    private String mMedia = "";
    private String mContent = "";
    private String mLat = "";
    private String mLon = "";

    /**
     * Get the title of this article
     * @return Title
     */
    public String getTitle() {
        return mTitle;
    }

    /**
     * Set the title of this article
     * @param title Title
     */
    public void setTitle(String title) {
        this.mTitle = title;
    }

    /**
     * Get the image url of this article
     * @return Image URL
     */
    public String getMedia() {
        return mMedia;
    }

    /**
     * Set the image url of this article
     * @param media Image URL
     */
    public void setMedia(String media) {
        this.mMedia = media;
    }

    /**
     * Get the content of this article
     * @return Content
     */
    public String getContent() {
        return mContent;
    }

    /**
     * Set the content of this article
     * @param content Content
     */
    public void setContent(String content) {
        this.mContent = content;
    }

    /**
     * Get the latitude of this article's event
     * @return Latitude
     */
    public String getLat() {
        return mLat;
    }

    /**
     * Set the latitude of this article's event
     * @param lat Latitude
     */
    public void setLat(String lat) {
        this.mLat = lat;
    }

    /**
     * Get the longitude of this article's event
     * @return Title
     */
    public String getLon() {
        return mLon;
    }

    /**
     * Set the longitude of this article's event
     * @param lon Longitude
     */
    public void setLon(String lon) {
        this.mLon = lon;
    }
}
