package de.tk.gomeekitest.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Model: Article
 *
 * This model represents one article entry in the article list view.
 * A list of this model is provided by {@see http://mobiletest.gomeekisystems.com/category_sport.json}
 */
public class Article implements Parcelable {

    private String mId = "";
    private String mDate = "";
    private Date mDateObj = null;
    private String mTitle = "";
    private String mShortDescription = "";
    private String mImage = "";

    /**
     * Standard constructor
     */
    public Article() {
    }

    /**
     * Get the ID
     * @return Article ID
     */
    public String getId() {
        return mId;
    }

    /**
     * Set the ID
     * @param id Article ID
     */
    public void setId(String id) {
        this.mId = id;
    }

    /**
     * Get the date of the article
     * @return Article date as @{link Date}
     */
    public Date getDateObject() {
        return mDateObj;
    }

    /**
     * Set the string representation of the article date
     * @param strDate Article date as String
     */
    public void setDate(String strDate) {
        this.mDate = strDate;

        DateFormat format = new SimpleDateFormat("yyyyMMdd'T'HHmmssZ", Locale.ENGLISH);
        try {
            Date date = format.parse(strDate);
            this.mDateObj = date;
        } catch (ParseException e) {
            this.mDateObj = null;
        }
    }


    /**
     * Get the string representation of the article date
     * @return Article date as string
     */
    public String getDate() {
        return mDate;
    }

    /**
     * Get the title
     * @return Article title
     */
    public String getTitle() {
        return mTitle;
    }

    /**
     * Set the title
     * @param title Article title
     */
    public void setTitle(String title) {
        this.mTitle = title;
    }

    /**
     * Get the short description
     * @return Article short description
     */
    public String getShortDescription() {
        return mShortDescription;
    }

    /**
     * Set the short description
     * @param shortDescription Article short description
     */
    public void setShortDescription(String shortDescription) {
        this.mShortDescription = shortDescription;
    }

    /**
     * Get the preview image URL
     * @return Article preview image url
     */
    public String getImage() {
        return mImage;
    }

    /**
     * Set the preview image URL
     * @param image Article preview image url
     */
    public void setImage(String image) {
        this.mImage = image;
    }

    /**
     * Make {@link Article} parcelable, in order to store it in the instance state.
     */
    public static final Parcelable.Creator<Article> CREATOR = new Parcelable.Creator<Article>() {
        public Article createFromParcel(Parcel source) {
            return new Article(source);
        }

        public Article[] newArray(int size) {
            return new Article[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mId);
        dest.writeString(this.mDate);
        dest.writeLong(mDateObj != null ? mDateObj.getTime() : -1);
        dest.writeString(this.mTitle);
        dest.writeString(this.mShortDescription);
        dest.writeString(this.mImage);
    }

    /**
     * Constructor for the parcelable creator
     * @param in
     */
    private Article(Parcel in) {
        this.mId = in.readString();
        this.mDate = in.readString();
        long tmpMDateObj = in.readLong();
        this.mDateObj = tmpMDateObj == -1 ? null : new Date(tmpMDateObj);
        this.mTitle = in.readString();
        this.mShortDescription = in.readString();
        this.mImage = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
